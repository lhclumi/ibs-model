from cpymad.madx import Madx
import yaml
import tree_maker
import sys
import json
import sys
import json
import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import matplotlib.pyplot as plt
import pandas as pd
from cpymad.madx import Madx
import scipy
from scipy.constants import e as qe
from scipy.constants import m_p, m_e
sys.path.append("/home/HPC/skostogl/workspace_Jul22/IBS_model_lookup_tables/michalis_ibs")
from ibs_lib.IBSfunctions import *

mp      = 938.272046 #MeV/c^2

def gammarel(EGeV,m0=mp):
  """returns the relativistic gamma
  Parameters:
  -----------
  EGeV: kinetic energy [GeV]
  m0: restmass [MeV]
  """
  return (EGeV*1.e3+m0)/m0

def betarel(EGeV,m0=mp):
  g=gammarel(EGeV,m0=m0)
  return np.sqrt(1-1/g**2)


def eta(alpha_c,EGeV,m0=mp):
  """calculates the phase slip factor
  and gamma transition where alpha_c
  is the momentum compaction factor"""
  g=(EGeV*1.e3+m0)/m0#gammarel(EGeV,m0=m0)
  eta=alpha_c-1/g**2
  gt=np.sqrt(1/np.abs(alpha_c))
  return eta,gt

#myeta  = eta(tw['momentum_compaction_factor'], 6800.0)
h = 35640
VMV = 12
EGeV = 6800.0
#circ = tw['circumference']

def bucket_height(h,eta,EGeV,VMV,m0=mp):
  """returns the bucket half heigth dE/E
  full bucket height reaches from [-dE/E,+dE/E]
  h: harmonic number
  eta: phase slip factor
  EGeV: beam energy [GeV]
  VMV: rf voltage [MV]
  m0: particle mass [MeV]
  """
  g=(EGeV*1.e3+m0)/m0
  b=np.sqrt(1-1/g**2)
  return b*np.sqrt((2*VMV*1.e6)/(np.pi*h*eta*EGeV*1.e9))



def EnergySpread(Circumferance, Harmonic_Num, Energy_total, SlipF, BL, beta_rel, RF_Voltage, Energy_loss, Z):
    '~~~ from Wiedermanns book ~~~'
    return BL / (Circumferance * np.sqrt(abs(SlipF) * Energy_total / (2 * np.pi * beta_rel * Harmonic_Num * np.sqrt(Z**2 * RF_Voltage**2 - Energy_loss**2))))


#EnergySpread(tw['circumference'], h, EGeV*1e9, eta(tw['momentum_compaction_factor'], 6800.0)[0], sigma_z, pco.beta0[0], VMV*1e6, 0, 1.0)

with open("config.yaml", "r") as f:
    config = yaml.safe_load(f)

n_studies = len(config["En"])
results = []

for study in range(n_studies):
  with open(config['sequence'][study]) as fid:
      dd=json.load(fid)
  line = xt.Line.from_dict(dd)

  pco = xp.Particles.from_dict(dd['particle_on_tracker_co'])
  
  tracker = xt.Tracker(line=line,
                      )
                      #extra_headers=['#define XTRACK_MULTIPOLE_NO_SYNRAD'])
  tw = tracker.twiss(particle_ref = pco)
  
  bunch_intensity = float(config['bunch_intensity'][study])
  sigma_ns        = float(config['blns'][study])
  sigma_z         = scipy.constants.speed_of_light * sigma_ns*1e-9/4.
  nemitt_x        = float(config['emit_x'][study])*1e-6
  nemitt_y        = float(config['emit_y'][study])*1e-6
  dt              = 5.0*60.0

  IBS = NagaitsevIBS()
  IBS.set_beam_parameters(pco)
  IBS.Npart = bunch_intensity
  if not np.isclose(EGeV, IBS.EnTot):
    IBS.EnTot=EGeV
    IBS.gammar = gammarel(EGeV)
    IBS.betar = betarel(EGeV)

  IBS.set_optic_functions(tw)


  emit_x_all   = []
  emit_y_all   = []
  sigma_z_all  = []
  txh          = []
  tyh          = []
  tlh          = []
  time         = []

  emit_x_all.append(nemitt_x)
  emit_y_all.append(nemitt_y)
  sigma_z_all.append(sigma_z) 
  txh.append(0.0)
  tyh.append(0.0)
  tlh.append(0.0)
  time.append(0.0)

  #for time_step in range(int(20*3600./dt)):
  for time_step in range(int(dt/dt)):
    emit_x         = emit_x_all[-1]/(IBS.betar*IBS.gammar)
    emit_y         = emit_y_all[-1]/(IBS.betar*IBS.gammar)
    
    Sigma_E = EnergySpread(tw['circumference'], h, IBS.EnTot*1e9, IBS.slip, sigma_z_all[-1], IBS.betar, VMV*1e6, 0, IBS.Ncharg)
    Sig_M = Sigma_E/IBS.betar**2
    IBS.calculate_integrals(emit_x,emit_y,Sig_M,sigma_z_all[-1])
    Emit_x, Emit_y, Sig_M = IBS.emit_evol(emit_x,emit_y,Sig_M,sigma_z_all[-1], dt)
    Sigma_E = Sig_M*IBS.betar**2
    #print(Emit_x*IBS.betar*IBS.gammar)
    #quit()
    BunchL = BunchLength(IBS.Circu, h, IBS.EnTot, IBS.slip,
                      Sigma_E, IBS.betar, VMV*1e-3, 0.0, IBS.Ncharg)

    print(Emit_x*IBS.betar*IBS.gammar, Emit_y*IBS.betar*IBS.gammar, BunchL)
    emit_x_all.append(Emit_x*IBS.betar*IBS.gammar)
    emit_y_all.append(Emit_y*IBS.betar*IBS.gammar)
    sigma_z_all.append(BunchL)
    print(time_step, 20*3600./dt)
    time.append( (time_step+1)*dt)
    txh.append(1.0/float(IBS.Ixx)/3600.0)
    tyh.append(1.0/float(IBS.Iyy)/3600.0)
    tlh.append(1.0/float(IBS.Ipp)/3600.0)
  df = pd.DataFrame({'en_x': emit_x_all, 'en_y':emit_y_all, 'sigma_z': sigma_z_all, 'txh': txh, 'tyh': tyh, 'tlh':tlh, 'time':time, 'bunch_intensity': bunch_intensity, 'sigma_ns':sigma_ns, 'emit_x0':nemitt_x,'emit_y0':nemitt_y})
  results.append(df)
results = pd.concat(results, axis=0)
results.to_parquet(f'{config["save_to"]}')
tree_maker.tag_json.tag_it(config['log_file'], 'completed')

